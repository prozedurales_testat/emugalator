# Who's who, and who's doing what?

## Base

#### CPU & MEM
  [x] data structure CPU p
  [x] interface p, l

  Instructions
    [x] ADD p
    [x] ADC p
    [x] SUB p
    [ ] SBC p
    [X] LD  l
    [X] LDD l
    [X] LDH l
    [ ] LDHL l
    [x] PUSH p
    [x] POP p
    [x] INC p
    [x] DEC p
    Logical operations
    [X] OR, AND, XOR p
    [x] CP j
    Miscellanious
    [0] SWAP, DAA, CPL l
    [X] CCF l
    [X] SCF l
    [x] NOP, HALT, STOP p
    [ ] DI l
    Rotates & shifts
    [ ] RLA, RLCA, RRA, RRCA p
    [ ] RLC, RL, RRC, RR j
    [0] SLA, SRA, SRL j
    Bit operations
    [ ] BIT, SET, RES p
    Jumps
    [X] JP, JR l
    Calls
    [ ] CALL p
    Restart etc
    [0] RST, RET, RETI l
Interrupts
    TODO: Add interrupt list



Graphics

0xC6 0x0f

#include <stdio.h>
#include <stdlib.h>

void bubble(int* l, int n){
    int a;
    for (int j = 0; j < n-1; j++) { for (int i = 0; i < n-1; i++) {
        if(l[i] > l[i+1]){
            a = l[i];
            l[i] = l[i+1];
            l[i+1] = a;
        }
    } }
}

void merge(int arr[], int l, int m, int r) {
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;
    int l1[n1], l2[n2];
    for (i = 0; i < n1; i++)
        l1[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        l2[j] = arr[m + 1 + j];
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2) {
        if (l1[i] <= l2[j]) {
            arr[k] = l1[i];
            i++;
        } else { arr[k] = l2[j]; j++; }
        k++;
    } while (i < n1) {
        arr[k] = l1[i];
        i++;
        k++;
    } while (j < n2) {
        arr[k] = l2[j];
        j++;
        k++;
    }
}
 
void msort(int arr[], int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;
        msort(arr, l, m);
        msort(arr, m + 1, r);
        merge(arr, l, m, r);
    }
}


int main(int argc, char const *argv[])
{
    int a[10] = {10,1,20,23,34,86,11,2,1,14};
    bubble(a,10);
    msort(a, 0, 9);
    for (int i = 0; i < 10; i++)
    {
        printf("%d, ", a[i]);
    }
    return 0;
}

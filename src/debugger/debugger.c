#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <curses.h>
#include <string.h>


//#include "instruction_lkup.c"
//#include "../emulator/instruction_lkup.c"
#include "insstr_to_op.c"


/****************************************************
 *               AUTHOR: PELLE HAENEL               *
 *                 DATE: 2020-12-29                 *
 * PURPOSE: PROVIDE AN IMPLEMENTATION OF A DEBUGGER *
 ****************************************************/


void print_i8(I8 v){
    I8* l = malloc(8*sizeof(I8));
    for (int i = 0; i < 8; i++){
        l[i] = v % 2;
        v /= 2;
    }
    printf("0b");
    for (int i = 0; i < 8; i++)
    {
        printf("%d", l[7-i]);
    }
}

void print_i16(I16 v){
    I8* l = malloc(16*sizeof(I16));
    for (int i = 0; i < 16; i++){
        l[i] = v % 2;
        v /= 2;
    }
    printf("0b");
    for (int i = 0; i < 16; i++)
    {
        printf("%d", l[15-i]);
    }
}

char** init_tokl(){
    char** tokl = malloc(10*sizeof(char*));
    for (int i = 0; i < 5; ++i) {
        tokl[i] = calloc(10, sizeof(char));
    } return tokl;
}

void tokenize_command(char* s, char** tokl){
    int i = 0;
    char* tok = strtok(s,",. ");
    while (tok != NULL) {
        tokl[i++] = tok;
        tok = strtok(NULL, ",. ");
    }
}


void enable_raw_mode(){
    initscr();
    noecho();
    cbreak();
    nodelay(stdscr, 1);
}

void disable_raw_mode(){
    nodelay(stdscr, 0);
    endwin();
}


void s_i8(I8 v, char* l){
    for (int i = 0; i < 8; i++){
        l[7-i] = '0' + v % 2;
        v /= 2;
    }
}

void s_i16(I16 v, char* l){
    for (int i = 0; i < 16; i++){
        l[15-i] = '0' + v % 2;
        v /= 2;
    }
}

void print_mem_section(CPU* cpu, WINDOW* w, I16 addr, int ypos, int xpos){
    /*************************************
     * PRINTS A SECTION OF THE GB MEMORY *
     *************************************/
    I16 addr_offset = addr - (addr % 16);
    char* cell = calloc(4, sizeof(char));
    char* mem  = calloc(8, sizeof(char));
    init_pair(1, COLOR_RED, COLOR_BLACK);
    werase(w);
    mvwprintw(w, ypos++, xpos,   "     |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f");
    mvwprintw(w, ypos++, xpos,   "-----+------------------------------------------------");
    for (int i = 0; i < 8; i++) {
        if(addr_offset - 3*16 + 16*i < 0){
            strcpy(mem, "#### | ");
        }else{
            sprintf(mem, "0x%x", addr_offset - 3*16 + 16*i);
            mem[4] = 0;
        }
        if(!(addr_offset - 3*16 + 16*i)){
            mvwprintw(w, ypos + i, xpos, "%s0 | ", mem);
        }else{
            mvwprintw(w, ypos + i, xpos, "%s | ", mem);}
        for (int j = 0; j < 16; j++) {
            if((addr_offset - 3*16 + 16*i + j) >= 0){ // - 1?
                sprintf(cell, "%x", read_mem(cpu, addr_offset - 3*16 + 16*i + j)); }
            else{ sprintf(cell, "##"); }
            if((addr_offset - 3*16 + 16*i + j) == addr){
                wattron(w, A_ITALIC);
                //mvwprintw(w, i + ypos , 3*j + xpos, "%s", cell);
                mvwprintw(w, i + ypos, 3*j + xpos + 7, "%s", cell);
                wattroff (w, A_ITALIC);
            }else{
            mvwprintw(w, i + ypos, 3*j + xpos + 7, "%s", cell);}
        }
    }
    free(cell);
    free(mem);
    wrefresh(w);
}



void print_regs(CPU* cpu, WINDOW* w, int y, int x){
    char* s  = malloc(8  * sizeof(char));
    char* ss = malloc(16 * sizeof(char));
    int n = 0;
    int v;

    werase(w);

    s_i8(get_register(cpu, A), s);
    mvwprintw(w, y + n  , x + 0 , "A:  0b%s", s);
    s_i8(get_register(cpu, F), s);
    mvwprintw(w, y + n++, x + 16, "F:  0b%s\n", s);

    s_i8(get_register(cpu, B), s);
    mvwprintw(w, y + n  , x + 0 , "B:  0b%s\n", s);
    s_i8(get_register(cpu, C), s);
    mvwprintw(w, y + n++, x + 16, "C:  0b%s\n", s);

    s_i8(get_register(cpu, D), s);
    mvwprintw(w, y + n  , x + 0 , "D:  0b%s\n", s);
    s_i8(get_register(cpu, E), s);
    mvwprintw(w, y + n++, x + 16, "E:  0b%s\n", s);

    s_i16(*(cpu->pc), ss);
    mvwprintw(w, y + n++  , x + 0 , "PC: 0b%s\n", ss);

    s_i16(*(cpu->sp), ss);
    mvwprintw(w, y + n++, x + 0, "SP: 0b%s\n", ss);

    v = get_register(cpu, A);
    mvwprintw(w, y + n  , x + 0 , "A:  0x%x", v);
    v = get_register(cpu, F);
    mvwprintw(w, y + n++, x + 16, "F:  0x%x\n", v);

    v = get_register(cpu, B);
    mvwprintw(w, y + n  , x + 0 , "B:  0x%x", v);
    v = get_register(cpu, C);
    mvwprintw(w, y + n++, x + 16, "C:  0x%x\n", v);

    v = get_register(cpu, D);
    mvwprintw(w, y + n  , x + 0 , "D:  0x%x", v);
    v = get_register(cpu, E);
    mvwprintw(w, y + n++, x + 16, "E:  0x%x\n", v);

    v = get_register(cpu, H);
    mvwprintw(w, y + n  , x + 0 , "H:  0x%x", v);
    v = get_register(cpu, L);
    mvwprintw(w, y + n++, x + 16, "L:  0x%x\n", v);

    mvwprintw(w, y + n, x + 0 , "SP: 0x%x", *(cpu->sp));
    mvwprintw(w, y + n, x + 16, "PC: 0x%x\n", *(cpu->pc));

    wrefresh(w);
}

void refresh_debugger(CPU* cpu, WINDOW* w1, WINDOW* w2, WINDOW* w3, int mempos, char op_ins_str[16], char* command){
    op_to_insstr(read_mem(cpu, mempos), op_ins_str);
    mvwprintw(w3, 0,0, "Opcode at address: 0x%x is: %s", mempos, op_ins_str);
    mvwprintw(w3, 1,0, "> %s", command);
    print_regs(cpu, w1, 2,0);
    mvwprintw(w1, 0,0, "REGISTERS:");
    mvwprintw(w1, 1,0, "----------");
    wrefresh(w1);
    print_mem_section(cpu, w2, mempos, 2, 0);
    mvwprintw(w2, 0,0, "MEMORY:");
    mvwprintw(w2, 1,0, "-------");
    wrefresh(w2);
    wrefresh(w3);
}

void debugger(CPU* cpu){
    char op_ins_str[16];
    int x, y, k = 0;
    int a = 0, aa = 0;
    int mempos = 0, op;
    char c = 0;
    char* command = calloc(40, sizeof(char));
    char** toklist = init_tokl();
    initscr();
    raw();
    noecho();
    start_color();
    getmaxyx(stdscr, y, x); //wtf
    WINDOW* w1 = newwin(12, 30, 0,0);
    WINDOW* w2 = newwin(12, 60, 0,32);
    WINDOW* w3 = newwin(3, x, 13,0);
    wrefresh(w1);
    //set_mem(cpu, 1, read_mem(cpu, 1));

    while(1){
        c = getch();
        if(k < 40){command[k++] = c;}
        if(c == 10){
            //command[k-1] = 0;
            k = 0;

            tokenize_command(command, toklist);
            werase(w3);

            //HANDLE FUNCTIONS
            //VM <I16>      -- Jump to memory address <I16>
            if(0 == strcmp(toklist[0], "VM")){
                sscanf(toklist[1], "%x", &mempos);
            }
            //EX <I8>       -- Execute opcode <I8>
            if(strcmp(toklist[0], "EX") == 0){
                sscanf(toklist[1], "%x", &op);
                execute_instruction(cpu, op);
            }

            //X <I8>       -- Xecoot instruction
            if(strcmp(toklist[0], "X") == 0){
                execute_instruction(cpu, insstr_to_op(command+2));
            }

            //MSET <I16> <I8> --set value <I8> at address <I16>
            if(strcmp(toklist[0], "MSET") == 0){
                sscanf(toklist[1], "%x", &a);
                sscanf(toklist[2], "%x", &aa);
                set_mem(cpu, a, aa);
            }

            //RSET <REG> <I8>
            if(strcmp(toklist[0], "RSET") == 0){
                sscanf(toklist[1], "%c", &a);
                sscanf(toklist[2], "%x", &aa);
                //set_mem(cpu, 0xff, 0xfa);
                if(a > 'F'){
                    switch (a)
                    {
                    case 'H':
                        set_register(cpu, H, aa);
                        break;
                    case 'L':
                        set_register(cpu, L, aa);
                        break;
                    case 'S':
                        *(cpu->sp) = aa;
                        break;
                    case 'P':
                        *(cpu->pc) = aa;
                        break;
                    default:
                        break;
                    }
                }else{
                set_register(cpu, a - 'A', aa);}
            }
            //RUN (Program)
            if(strcmp(toklist[0], "RUN") == 0 || strcmp(toklist[0], "R") ==0 ){
                while(read_mem(cpu, *cpu->pc) != 0x10)
                    execute_instruction(cpu, read_mem(cpu, *cpu->pc));
                //is this literally it?
            }
            //WALK (RUN but with steps :^)  )
            if(strcmp(toklist[0], "WALK") == 0 || strcmp(toklist[0], "W") == 0){
                while(read_mem(cpu, *cpu->pc) != 0x10){
                    refresh_debugger(cpu, w1, w2, w3, mempos, op_ins_str, command);
                    execute_instruction(cpu, read_mem(cpu, *cpu->pc));
                    if(getchar() == 'q') break;
                }
            }
            memset(command, 0, 40);
        }
        if(c == 27) {endwin(); nodelay(stdscr, false); return;}
        refresh_debugger(cpu, w1, w2, w3, mempos, op_ins_str, command);
    }
    endwin();
    nodelay(stdscr, false);
}

void set_Example(CPU* cpu){
    set_mem(cpu, 0x00, 0x01);
    set_mem(cpu, 0x01, 0x02);
    set_mem(cpu, 0x02, 0x01);
    set_mem(cpu, 0x03, 0x21);
    set_mem(cpu, 0x06, 0x2A);
    set_mem(cpu, 0x07, 0x80);
    set_mem(cpu, 0x08, 0x47);
    set_mem(cpu, 0x09, 0xFE);
    set_mem(cpu, 0x0A, 0x04);
    set_mem(cpu, 0x0B, 0xC2);
    set_mem(cpu, 0x0C, 0x06);
    set_mem(cpu, 0x0E, 0x10);
    set_register(cpu , PC, 0x03);
}

int main(int argc, char const *argv[]){
    CPU* cpu = init_cpu();
    set_Example(cpu);
    debugger(cpu);
    return 0;
}

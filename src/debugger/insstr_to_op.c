#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../emulator/instruction_lkup.c"

I8 insstr_to_op(char* ins){
    if(strcmp("LD B,n", ins) == 0) return 0x06;
    if(strcmp("LD C,n", ins) == 0) return 0x0E;
    if(strcmp("LD D,n", ins) == 0) return 0x16;
    if(strcmp("LD E,n", ins) == 0) return 0x1E;
    if(strcmp("LD H,n", ins) == 0) return 0x26;
    if(strcmp("LD L,n", ins) == 0) return 0x2E;
    if(strcmp("LD A,A", ins) == 0) return 0x7F;
    if(strcmp("LD A,B", ins) == 0) return 0x78;
    if(strcmp("LD A,C", ins) == 0) return 0x79;
    if(strcmp("LD A,D", ins) == 0) return 0x7A;
    if(strcmp("LD A,E", ins) == 0) return 0x7B;
    if(strcmp("LD A,H", ins) == 0) return 0x7C;
    if(strcmp("LD A,L", ins) == 0) return 0x7D;
    if(strcmp("LD B,B", ins) == 0) return 0x40;
    if(strcmp("LD B,C", ins) == 0) return 0x41;
    if(strcmp("LD B,D", ins) == 0) return 0x42;
    if(strcmp("LD B,E", ins) == 0) return 0x43;
    if(strcmp("LD B,H", ins) == 0) return 0x44;
    if(strcmp("LD B,L", ins) == 0) return 0x45;
    if(strcmp("LD C,B", ins) == 0) return 0x48;
    if(strcmp("LD C,C", ins) == 0) return 0x49;
    if(strcmp("LD C,D", ins) == 0) return 0x4A;
    if(strcmp("LD C,E", ins) == 0) return 0x4B;
    if(strcmp("LD C,H", ins) == 0) return 0x4C;
    if(strcmp("LD C,L", ins) == 0) return 0x4D;
    if(strcmp("LD D,B", ins) == 0) return 0x50;
    if(strcmp("LD D,C", ins) == 0) return 0x51;
    if(strcmp("LD D,D", ins) == 0) return 0x52;
    if(strcmp("LD D,E", ins) == 0) return 0x53;
    if(strcmp("LD D,H", ins) == 0) return 0x54;
    if(strcmp("LD D,L", ins) == 0) return 0x55;
    if(strcmp("LD E,B", ins) == 0) return 0x58;
    if(strcmp("LD E,C", ins) == 0) return 0x59;
    if(strcmp("LD E,D", ins) == 0) return 0x5A;
    if(strcmp("LD E,E", ins) == 0) return 0x5B;
    if(strcmp("LD E,H", ins) == 0) return 0x5C;
    if(strcmp("LD E,L", ins) == 0) return 0x5D;
    if(strcmp("LD H,B", ins) == 0) return 0x60;
    if(strcmp("LD H,C", ins) == 0) return 0x61;
    if(strcmp("LD H,D", ins) == 0) return 0x62;
    if(strcmp("LD H,E", ins) == 0) return 0x63;
    if(strcmp("LD H,H", ins) == 0) return 0x64;
    if(strcmp("LD H,L", ins) == 0) return 0x65;
    if(strcmp("LD L,B", ins) == 0) return 0x68;
    if(strcmp("LD L,C", ins) == 0) return 0x69;
    if(strcmp("LD L,D", ins) == 0) return 0x6A;
    if(strcmp("LD L,E", ins) == 0) return 0x6B;
    if(strcmp("LD L,H", ins) == 0) return 0x6C;
    if(strcmp("LD L,L", ins) == 0) return 0x6D;
    if(strcmp("LD A,A", ins) == 0) return 0x7F;
    if(strcmp("LD A,B", ins) == 0) return 0x78;
    if(strcmp("LD A,C", ins) == 0) return 0x79;
    if(strcmp("LD A,D", ins) == 0) return 0x7A;
    if(strcmp("LD A,E", ins) == 0) return 0x7B;
    if(strcmp("LD A,H", ins) == 0) return 0x7C;
    if(strcmp("LD A,L", ins) == 0) return 0x7D;
    if(strcmp("LD A,#", ins) == 0) return 0x3E;
    if(strcmp("LD A,A", ins) == 0) return 0x7F;
    if(strcmp("LD B,A", ins) == 0) return 0x47;
    if(strcmp("LD C,A", ins) == 0) return 0x4F;
    if(strcmp("LD D,A", ins) == 0) return 0x57;
    if(strcmp("LD E,A", ins) == 0) return 0x5F;
    if(strcmp("LD H,A", ins) == 0) return 0x67;
    if(strcmp("LD L,A", ins) == 0) return 0x6F;
}

void op_to_insstr(I8 op, char* res){
    switch (op) {
        case 0x06: strcpy(res, "LD B,n"); break;
        case 0x0E: strcpy(res, "LD C,n"); break;
        case 0x16: strcpy(res, "LD D,n"); break;
        case 0x1E: strcpy(res, "LD E,n"); break;
        case 0x26: strcpy(res, "LD H,n"); break;
        case 0x2E: strcpy(res, "LD L,n"); break;
        case 0x7F: strcpy(res, "LD A,A"); break;
        case 0x78: strcpy(res, "LD A,B"); break;
        case 0x79: strcpy(res, "LD A,C"); break;
        case 0x7A: strcpy(res, "LD A,D"); break;
        case 0x7B: strcpy(res, "LD A,E"); break;
        case 0x7C: strcpy(res, "LD A,H"); break;
        case 0x7D: strcpy(res, "LD A,L"); break;
        case 0x40: strcpy(res, "LD B,B"); break;
        case 0x41: strcpy(res, "LD B,C"); break;
        case 0x42: strcpy(res, "LD B,D"); break;
        case 0x43: strcpy(res, "LD B,E"); break;
        case 0x44: strcpy(res, "LD B,H"); break;
        case 0x45: strcpy(res, "LD B,L"); break;
        case 0x48: strcpy(res, "LD C,B"); break;
        case 0x49: strcpy(res, "LD C,C"); break;
        case 0x4A: strcpy(res, "LD C,D"); break;
        case 0x4B: strcpy(res, "LD C,E"); break;
        case 0x4C: strcpy(res, "LD C,H"); break;
        case 0x4D: strcpy(res, "LD C,L"); break;
        case 0x50: strcpy(res, "LD D,B"); break;
        case 0x51: strcpy(res, "LD D,C"); break;
        case 0x52: strcpy(res, "LD D,D"); break;
        case 0x53: strcpy(res, "LD D,E"); break;
        case 0x54: strcpy(res, "LD D,H"); break;
        case 0x55: strcpy(res, "LD D,L"); break;
        case 0x58: strcpy(res, "LD E,B"); break;
        case 0x59: strcpy(res, "LD E,C"); break;
        case 0x5A: strcpy(res, "LD E,D"); break;
        case 0x5B: strcpy(res, "LD E,E"); break;
        case 0x5C: strcpy(res, "LD E,H"); break;
        case 0x5D: strcpy(res, "LD E,L"); break;
        case 0x60: strcpy(res, "LD H,B"); break;
        case 0x61: strcpy(res, "LD H,C"); break;
        case 0x62: strcpy(res, "LD H,D"); break;
        case 0x63: strcpy(res, "LD H,E"); break;
        case 0x64: strcpy(res, "LD H,H"); break;
        case 0x65: strcpy(res, "LD H,L"); break;
        case 0x68: strcpy(res, "LD L,B"); break;
        case 0x69: strcpy(res, "LD L,C"); break;
        case 0x6A: strcpy(res, "LD L,D"); break;
        case 0x6B: strcpy(res, "LD L,E"); break;
        case 0x6C: strcpy(res, "LD L,H"); break;
        case 0x6D: strcpy(res, "LD L,L"); break;
        case 0x3E: strcpy(res, "LD A,n"); break;
        case 0x47: strcpy(res, "LD B,A"); break;
        case 0x4F: strcpy(res, "LD C,A"); break;
        case 0x57: strcpy(res, "LD D,A"); break;
        case 0x5F: strcpy(res, "LD E,A"); break;
        case 0x67: strcpy(res, "LD H,A"); break;
        case 0x6F: strcpy(res, "LD L,A"); break;
        default: strcpy(res, "#"); break;
    }
}

#include <stdio.h>
#include <stdlib.h>

#include "instructions.c"

typedef I16 Opcode;

void execute_instruction(CPU* cpu, Opcode op){
    switch (op){
    //ADD8 A , REG8/IMM
    case 0x87: ins_ADD(cpu, A); break;
    case 0x80: ins_ADD(cpu, B); break;
    case 0x81: ins_ADD(cpu, C); break;
    case 0x82: ins_ADD(cpu, D); break;
    case 0x83: ins_ADD(cpu, E); break;
    case 0x84: ins_ADD(cpu, H); break;
    case 0x85: ins_ADD(cpu, L); break;
    case 0x86: ins_ADD(cpu, RHL); break;
    case 0xC6: ins_ADD(cpu, N); break;
    //ADD16 HL, REG16
    case 0x09: ins_ADD(cpu, BC); break;
    case 0x19: ins_ADD(cpu, DE); break;
    case 0x29: ins_ADD(cpu, HL); break;
    case 0x39: ins_ADD(cpu, SP); break;
    //INC n
    case 0x3C: ins_INC(cpu, A); break;
    case 0x04: ins_INC(cpu, B); break;
    case 0x0C: ins_INC(cpu, C); break;
    case 0x14: ins_INC(cpu, D); break;
    case 0x1C: ins_INC(cpu, E); break;
    case 0x24: ins_INC(cpu, H); break;
    case 0x2C: ins_INC(cpu, L); break;
    case 0x34: ins_INC(cpu, RHL); break;
    //DEC n
    case 0x3D: ins_DEC(cpu, A); break;
    case 0x05: ins_DEC(cpu, B); break;
    case 0x0D: ins_DEC(cpu, C); break;
    case 0x15: ins_DEC(cpu, D); break;
    case 0x1D: ins_DEC(cpu, E); break;
    case 0x25: ins_DEC(cpu, H); break;
    case 0x2D: ins_DEC(cpu, L); break;
    case 0x35: ins_DEC(cpu, RHL); break;
    //LD8 Case 1,2,3,4,5
    //1:
    case 0x06: ins_LD8(cpu, B, N); break;
    case 0x0E: ins_LD8(cpu, C, N); break;
    case 0x16: ins_LD8(cpu, D, N); break;
    case 0x1E: ins_LD8(cpu, E, N); break;
    case 0x26: ins_LD8(cpu, H, N); break;
    case 0x2E: ins_LD8(cpu, L, N); break;
    //2:
    //  A:
    case 0x7F: ins_LD8(cpu, A, A); break;
    case 0x78: ins_LD8(cpu, A, B); break;
    case 0x79: ins_LD8(cpu, A, C); break;
    case 0x7A: ins_LD8(cpu, A, D); break;
    case 0x7B: ins_LD8(cpu, A, E); break;
    case 0x7C: ins_LD8(cpu, A, H); break;
    case 0x7D: ins_LD8(cpu, A, L); break;
    case 0x7E: ins_LD8(cpu, A, RHL); break;
    //  B:
    case 0x40: ins_LD8(cpu, B, B); break;
    case 0x41: ins_LD8(cpu, B, C); break;
    case 0x42: ins_LD8(cpu, B, D); break;
    case 0x43: ins_LD8(cpu, B, E); break;
    case 0x44: ins_LD8(cpu, B, H); break;
    case 0x45: ins_LD8(cpu, B, L); break;
    case 0x46: ins_LD8(cpu, B, RHL); break;
    //  C:
    case 0x48: ins_LD8(cpu, C, B); break;
    case 0x49: ins_LD8(cpu, C, C); break;
    case 0x4A: ins_LD8(cpu, C, D); break;
    case 0x4B: ins_LD8(cpu, C, E); break;
    case 0x4C: ins_LD8(cpu, C, H); break;
    case 0x4D: ins_LD8(cpu, C, L); break;
    case 0x4E: ins_LD8(cpu, C, RHL); break;
    //  D:
    case 0x50: ins_LD8(cpu, D, B); break;
    case 0x51: ins_LD8(cpu, D, C); break;
    case 0x52: ins_LD8(cpu, D, D); break;
    case 0x53: ins_LD8(cpu, D, E); break;
    case 0x54: ins_LD8(cpu, D, H); break;
    case 0x55: ins_LD8(cpu, D, L); break;
    case 0x56: ins_LD8(cpu, D, RHL); break;
    //  E:
    case 0x58: ins_LD8(cpu, E, B); break;
    case 0x59: ins_LD8(cpu, E, C); break;
    case 0x5A: ins_LD8(cpu, E, D); break;
    case 0x5B: ins_LD8(cpu, E, E); break;
    case 0x5C: ins_LD8(cpu, E, H); break;
    case 0x5D: ins_LD8(cpu, E, L); break;
    case 0x5E: ins_LD8(cpu, E, RHL); break;
    //  H:
    case 0x60: ins_LD8(cpu, H, B); break;
    case 0x61: ins_LD8(cpu, H, C); break;
    case 0x62: ins_LD8(cpu, H, D); break;
    case 0x63: ins_LD8(cpu, H, E); break;
    case 0x64: ins_LD8(cpu, H, H); break;
    case 0x65: ins_LD8(cpu, H, L); break;
    case 0x66: ins_LD8(cpu, H, RHL); break;
    //  L:
    case 0x68: ins_LD8(cpu, L ,B); break;
    case 0x69: ins_LD8(cpu, L, C); break;
    case 0x6A: ins_LD8(cpu, L, D); break;
    case 0x6B: ins_LD8(cpu, L, E); break;
    case 0x6C: ins_LD8(cpu, L, H); break;
    case 0x6D: ins_LD8(cpu, L, L); break;
    case 0x6E: ins_LD8(cpu, L, RHL); break;
    //  HL:
    case 0x70: ins_LD8(cpu, RHL, B); break;
    case 0x71: ins_LD8(cpu, RHL, C); break;
    case 0x72: ins_LD8(cpu, RHL, D); break;
    case 0x73: ins_LD8(cpu, RHL, E); break;
    case 0x74: ins_LD8(cpu, RHL, H); break;
    case 0x75: ins_LD8(cpu, RHL, L); break;
    case 0x36: ins_LD8(cpu, RHL, N); break;
    //3:
    case 0x0A: ins_LD8(cpu, A, RBC); break;
    case 0x1A: ins_LD8(cpu, A, RDE); break;
    case 0xFA: ins_LD8(cpu, A, NN); break;//TODO: Look up Use for (nn)
    case 0x3E: ins_LD8(cpu, A, N); break;
    //4:
    case 0x47: ins_LD8(cpu, B, A); break;
    case 0x4F: ins_LD8(cpu, C, A); break;
    case 0x57: ins_LD8(cpu, D, A); break;
    case 0x5F: ins_LD8(cpu, E, A); break;
    case 0x67: ins_LD8(cpu, H, A); break;
    case 0x6F: ins_LD8(cpu, L, A); break;
    case 0x02: ins_LD8(cpu, RBC, A); break;
    case 0x12: ins_LD8(cpu, RDE, A); break;
    case 0x77: ins_LD8(cpu, RHL, A); break;
    case 0xEA: ins_LD8(cpu, NN, A); break;
    //5:
    case 0xF2: ins_LD8_get_mem(cpu, A, C, 0xFF00); break;
    //6:
    case 0xE2: ins_LD8_mem(cpu, C, A, 0xFF00); break;
    //7,8,9:
    case 0x3A: ins_LDD(cpu, A, RHL); break;
    //10,11,12:
    case 0x32: ins_LDD_mem(cpu, RHL, A); break;
    //13,14,15:
    case 0x2A: ins_LDI(cpu); break;
    //16,17,18:
    case 0x22: ins_LDI_mem(cpu, HL, A); break;
    //19:
    case 0xE0: ins_LD8_mem(cpu, N, A, 0xFF00); break;
    //20:
    case 0xF0: ins_LD8_get_mem(cpu, A, N, 0xFF00); break;
    //LD 16B:
    //  1:
    case 0x01: ins_LD16(cpu, BC, NN); break;
    case 0x11: ins_LD16(cpu, DE, NN); break;
    case 0x21: ins_LD16(cpu, HL, NN); break;
    case 0x31: set_register(cpu, SP, get_register(cpu, NN)); break;
    //  2:
    case 0xF9: set_register(cpu, SP, get_register(cpu, HL)); break;
    //  3,4:
    case 0xF8: ins_LDHL_SP(cpu); break;
    //  5:
    case 0x08: ins_LD16_mem(cpu, NN, SP,0); break; //TODO: Need extra clearence
    //RLCA:
    case 0x07: ins_RLCA(cpu); break;

    //Misc:
    //  1: // SWAP
    //  2:
    case 0x27: break;
    //  3:
    case 0x2F: ins_CPL(cpu); break;
    //  4:
    case 0x3F: ins_CCF(cpu); break;
    //  5:
    case 0x37: ins_SCF(cpu); break;
    //AND (8-Bit)
    case 0xA7: ins_AND(cpu, A); break;
    case 0xA0: ins_AND(cpu, B); break;
    case 0xA1: ins_AND(cpu, C); break;
    case 0xA2: ins_AND(cpu, D); break;
    case 0xA3: ins_AND(cpu, E); break;
    case 0xA4: ins_AND(cpu, H); break;
    case 0xA5: ins_AND(cpu, L); break;
    case 0xA6: ins_AND(cpu, RHL); break;
    case 0xE6: ins_AND(cpu, N); break;
    //OR (8-Bit)
    case 0xB7: ins_OR(cpu, A); break;
    case 0xB0: ins_OR(cpu, B); break;
    case 0xB1: ins_OR(cpu, C); break;
    case 0xB2: ins_OR(cpu, D); break;
    case 0xB3: ins_OR(cpu, E); break;
    case 0xB4: ins_OR(cpu, H); break;
    case 0xB5: ins_OR(cpu, L); break;
    case 0xB6: ins_OR(cpu, RHL); break;
    case 0xF6: ins_OR(cpu, N); break;
    //XOR (8-Bit)
    case 0xAF: ins_XOR(cpu, A); break;
    case 0xA8: ins_XOR(cpu, B); break;
    case 0xA9: ins_XOR(cpu, C); break;
    case 0xAA: ins_XOR(cpu, D); break;
    case 0xAB: ins_XOR(cpu, E); break;
    case 0xAC: ins_XOR(cpu, H); break;
    case 0xAD: ins_XOR(cpu, L); break;
    case 0xAE: ins_XOR(cpu, RHL); break;
    case 0xEE: ins_XOR(cpu, N); break;
    //CP
    case 0xBF: ins_CP(cpu, A); break;
    case 0xB8: ins_CP(cpu, B); break;
    case 0xB9: ins_CP(cpu, C); break;
    case 0xBA: ins_CP(cpu, D); break;
    case 0xBB: ins_CP(cpu, E); break;
    case 0xBC: ins_CP(cpu, H); break;
    case 0xBD: ins_CP(cpu, L); break;
    case 0xBE: ins_CP(cpu, RHL); break;
    case 0xFE: ins_CP(cpu, N); break;
    //3.3.6 Rotates and Shifts
    //  1:
    //3.3.8 Jumps:
    //  1:
    case 0xC3: ins_JP(cpu); break;
    //  2:
    case 0xC2: ins_JP_OP(cpu, nn, A); break;
    case 0xCA: ins_JP_OP(cpu, zz, A); break;
    case 0xD2: ins_JP_OP(cpu, hh, A); break;
    case 0xDA: ins_JP_OP(cpu, cc, A); break;
    //  3:
    case 0xE9: ins_JP_OP(cpu, nn, HL); break;
    //  4:
    case 0x18: ins_JR(cpu); break;
    //  5:
    case 0x20: ins_JR_OP(cpu, nn); break;
    case 0x28: ins_JR_OP(cpu, zz); break;
    case 0x30: ins_JR_OP(cpu, hh); break;
    case 0x38: ins_JR_OP(cpu, cc); break;
    //3.3.10 Restart:
    case 0xC7: ins_RST(cpu, 0x00); break;
    case 0xCF: ins_RST(cpu, 0x08); break;
    case 0xD7: ins_RST(cpu, 0x10); break;
    case 0xDF: ins_RST(cpu, 0x18); break;
    case 0xE7: ins_RST(cpu, 0x20); break;
    case 0xEF: ins_RST(cpu, 0x28); break;
    case 0xF7: ins_RST(cpu, 0x30); break;
    case 0xFF: ins_RST(cpu, 0x38); break;
    //3.3.11 Returns:
    //  1:
    case 0xC9: ins_RET(cpu); break;
    //  2:
    case 0xC0: ins_RET_OP(cpu, nn); break;
    case 0xC8: ins_RET_OP(cpu, zz); break;
    case 0xD0: ins_RET_OP(cpu, hh); break;
    case 0xD8: ins_RET_OP(cpu, cc); break;
    //  3:
    case 0xD9: ins_RET(cpu); break; //TODO Enabe Interrupts
    //PUSHs
    case 0xF5: ins_PUSH(cpu, AF); break;
    case 0xC5: ins_PUSH(cpu, BC); break;
    case 0xD5: ins_PUSH(cpu, DE); break;
    case 0xE5: ins_PUSH(cpu, HL); break;
    //POPs
    default:
        break;
    }
    increase_pc(cpu);
}

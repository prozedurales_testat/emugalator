#include <stdio.h>
#include <stdlib.h>

#include "processor_mem.c"

/*********************************************************************
 *               AUTHOR: LEONARD KRUSCH, PELLE HAENEL                *
 *                         DATE: 2020-12-29                          *
 * PURPOSE: THIS IMPLEMENTS THE ASM-INSTRUCTIONS FOR THE GAMEBOY CPU *
 *********************************************************************/

void ins_ADD(CPU* cpu, Reg r){  //P
    I16 v = get_register(cpu, r);
    I16 a = get_register(cpu, A);
    //check for nibble carry
    (((v & 0xf) + (a & 0xf)) & 0x10)? set_flag(cpu, hh, 1): set_flag(cpu, hh, 0);
    //check for carry
    ((v + a) & 0x100) ? set_flag(cpu, cc, 1) : set_flag(cpu, cc, 0);
    //perform addition and mask off upper byte
    a = (a + v) & 0xff;
    //check if result is zero and set zero flag accordingly
    (!a) ? set_flag(cpu,zz,1) : set_flag(cpu, zz, 0);
    set_flag(cpu, nn, 0);
    set_register(cpu, A, a);
}

void ins_ADC(CPU* cpu, Reg r){  //P
    I16 v = get_register(cpu, r);
    I16 a = get_register(cpu, A);
    I8 c = get_flag(cpu, cc);
    //check for nibble carry
    (((v & 0xf) + (a & 0xf) + c) & 0x10)? set_flag(cpu, hh, 1): set_flag(cpu, hh, 0);
    //check for carry
    ((v + a + c) & 0x100) ? set_flag(cpu, cc, 1) : set_flag(cpu, cc, 0);
    //perform addition and mask off upper byte
    a = (a + v + c) & 0xff;
    //check if result is zero and set zero flag accordingly
    (!a) ? set_flag(cpu,zz,1) : set_flag(cpu, zz, 0);
    set_flag(cpu, nn, 0);
    set_register(cpu, A, a);
}
/*************
 * 3.3 LOADS *
 *************/

void ins_LD8(CPU* cpu, Reg r, Reg rr){ //L
    set_register(cpu, r, get_register(cpu, rr) & 0x00FF);
}

void ins_LD8_mem(CPU* cpu, Reg r, Reg rr, I16 offset){  //L
    set_mem(cpu, get_register(cpu, r) + offset, (I8) get_register(cpu, rr));
}

void ins_LD8_get_mem(CPU* cpu, Reg r, Reg rr, I16 offset){  //L
    set_register(cpu, r, read_mem(cpu, get_register(cpu, rr) + offset));
}

void ins_LD16(CPU* cpu, Reg r, Reg rr){ //L
    set_register(cpu, r, get_register(cpu, rr));
}

void ins_LD16_mem(CPU* cpu, Reg r, Reg rr, I16 offset){  //L
    set_mem(cpu, get_register(cpu, r) + offset,get_register(cpu, rr));
}

void ins_LD16_get_mem(CPU* cpu, Reg r, Reg rr, I16 offset){  //L
    set_register(cpu, r, read_mem(cpu, get_register(cpu, rr) + offset));
}

void ins_LDD(CPU* cpu, Reg r, Reg rr){ //L
    set_register(cpu, r, get_register(cpu, rr));
    ins_DEC(cpu, rr); //FIXME: TEST FOR POINTER DEC
}

void ins_LDD_mem(CPU* cpu, Reg r, Reg rr){ //L
    set_mem(cpu, get_register(cpu, r), get_register(cpu, rr));
    ins_DEC(cpu, rr);
}

void ins_LDI(CPU* cpu){ //L
    set_register(cpu, A, get_register(cpu, RHL));
    ins_INC(cpu, HL);
}

void ins_LDI_mem(CPU* cpu, Reg r, Reg rr){ //L
    set_mem(cpu, get_register(cpu, r), get_register(cpu, rr));
    ins_INC(cpu, rr);
}

void ins_LDHL_SP(CPU* cpu){ //L
    ins_ADD16(cpu, SP, N);
    set_flag(cpu, zz, 0);
}

void ins_RLCA(CPU* cpu){    //P
    I16 a = get_register(cpu, A);
    a = a << 1;
    a = a + ((a & 0x100) >> 8);
    set_flag(cpu, cc, a & 0x01);
    a == 0 ? set_flag(cpu, zz, 1) : set_flag(cpu, zz, 0);
    set_flag(cpu, nn, 0);
    set_flag(cpu, hh, 0);
    set_register(cpu, A, a);
    clean_register(cpu);
}

 void ins_ADD16(CPU* cpu, Reg r, Reg rr){    //P
    I32 a = get_register(cpu, r);
    I32 v = get_register(cpu, rr);
    //check for nibble carry
    if((v & 0xfff) + (a & 0xfff) & 0x1000){set_flag(cpu, hh, 1);}else{set_flag(cpu, hh, 0);} //FIXME: reset if nothing?
    //check for carry
    if((v + a) & 0x10000) set_flag(cpu, cc, 1);   //FIXME: reset if false?
    //perform addition and mask off upper byte
    a = (a + v) & 0xffff;
    //check if result is zero and set zero flag accordingly
    if(!a) set_flag(cpu,zz,1);
    //reset n flag TODO: check if zero is correct for a reset
    set_flag(cpu, nn, 0);
    set_register(cpu, HL, a);
}

void ins_INC(CPU* cpu,Reg r){
    I16 v = get_register(cpu, r);
    v+=1;
    (v & 0xFF)==0 ? set_flag(cpu,zz,1):set_flag(cpu, zz, 0);
    set_register(cpu,r,v & 0xFF);
    set_flag(cpu,nn,0);
    (((v & 0xf) + 1) & 0x10) ? set_flag(cpu, hh, 1): set_flag(cpu, hh, 0);
}

void ins_DEC(CPU* cpu,Reg r){
    I16 v = get_register(cpu, r);
    if(v == 0){v = 0xFF;} //TODO: Check this thoroughly
    v-=1;
    v == 0 ? set_flag(cpu,zz,1): set_flag(cpu, zz, 0);
    set_register(cpu,r,v);
    set_flag(cpu,nn,0);
    set_flag(cpu, hh, 0);
}

void ins_AND(CPU* cpu, Reg r){
    I16 a = get_register(cpu, A);
    I16 v = get_register(cpu, r);
    a &= v;
    set_flag(cpu, hh, 1);
    set_flag(cpu, nn, 0);
    set_flag(cpu, cc, 0);
    set_flag(cpu, zz, (!a)%1);
    set_register(cpu, A, a);
}

void ins_OR(CPU* cpu, Reg r){
    I16 a = get_register(cpu, A);
    I16 v = get_register(cpu, r);
    a |= v;
    set_flag(cpu, hh, 0);
    set_flag(cpu, nn, 0);
    set_flag(cpu, cc, 0);
    set_flag(cpu, zz, (!a)%1);
    set_register(cpu, A, a);
}

void ins_XOR(CPU* cpu, Reg r){
    I16 a = get_register(cpu, A);
    I16 v = get_register(cpu, r);
    a ^= v;
    set_flag(cpu, hh, 0);
    set_flag(cpu, nn, 0);
    set_flag(cpu, cc, 0);
    set_flag(cpu, zz, (!a)%1);
    set_register(cpu, A, a);
}

void ins_CP(CPU* cpu, Reg r){ //J,P
    I16 a = get_register(cpu, A);
    I16 n = get_register(cpu, r);
    (a<n) ? set_flag(cpu, cc, 1) : set_flag(cpu, cc, 0);
    a -= n;
    (!a) ? set_flag(cpu, zz, 1) : set_flag(cpu, zz, 0);
    set_flag(cpu, nn, 1);
    (a>=16) ? set_flag(cpu, hh, 0): set_flag(cpu, hh, 1); //FIXME debug
    set_flag(cpu, hh, ((a & 0x0F) < (n & 0x0F)));
}

void ins_SUB(CPU* cpu, Reg r){ //P
    I16 a = get_register(cpu, A);
    I16 n = get_register(cpu, r);
    (a<n) ? set_flag(cpu, cc, 1) : set_flag(cpu, cc, 0);
    a -= n;
    (!a) ? set_flag(cpu, zz, 1) : set_flag(cpu, zz, 0);
    set_flag(cpu, nn, 1);
    (a>=16) ? set_flag(cpu, hh, 0): set_flag(cpu, hh, 1); //FIXME debug
    set_flag(cpu, hh, ((a & 0x0F) < (n & 0x0F)));
    set_register(cpu, r, a);
}
/**************
 * 3.3.5 MISC *
 **************/
//1:
void ins_SWAP(CPU* cpu, Reg r){ //L
    I16 a = get_register(cpu, r) << 4;
    a = (0xFF & a) + ((0xF00 & a) >> 8);
    if (!a) set_flag(cpu, zz, 1);
    set_register(cpu, r, a);
}

//2:
// https://www.reddit.com/r/Gameboy/comments/4m7sm0/technical_how_did_old_gameboy_games_represent/
//3:
void ins_CPL(CPU* cpu){ //L
    set_flag(cpu, nn, 1);
    set_flag(cpu, hh, 1);
    I8 a = get_register(cpu, A);
    a = ~a;
    set_register(cpu, A, a);
}

//4:
void ins_CCF(CPU* cpu){ //L
    I8 v = get_register(cpu, F);
    v = v ^ 0x10;
    set_register(cpu, F, v);
    set_flag(cpu, nn, 0);
    set_flag(cpu, hh, 0);

}

//5:
void ins_SCF(CPU* cpu){ //L
    set_flag(cpu, cc, 1);
    set_flag(cpu, nn, 0);
    set_flag(cpu, hh, 0);
}

/****************************
 * 3.3.6 ROTATES AND SHIFTS *
 ****************************/

void ins_SLA(CPU* cpu,Reg r){
    I16 v = get_register(cpu, r);
    v = v << 1;
    set_register(cpu, r, v);
    !v ? set_flag(cpu, zz, 1) : set_flag(cpu, zz, 0);
    set_flag(cpu, nn, 0);
    set_flag(cpu, hh, 0);
    set_flag(cpu, cc, (v>>8)&0x1);
    

}


void ins_SRA(CPU* cpu, Reg r){
    I8 v = get_register(cpu, r);
    int a = 0; 
    v%2 ? set_flag(cpu, cc, 0) : set_flag(cpu, cc, 1);
    if(v>=128) a = 128;
    v = v >> 1;
    v += a;
    set_register(cpu, r, v);
    set_flag(cpu, nn, 0);
    set_flag(cpu, hh, 0);
    set_flag(cpu, zz, (!v)%1);
    
}


void ins_SRL(CPU* cpu, Reg r){
    I8 v = get_register(cpu,r);
    v%2 ? set_flag(cpu, cc, 0) : set_flag(cpu, cc, 1);
    set_flag(cpu, nn, 0);
    set_flag(cpu, hh, 0);
    set_flag(cpu, zz, (!v)%1);
    v=v>>1;
    set_register(cpu,r ,v);


}

/***************
 * 3.3.8 JUMPS *
 ***************/

void ins_JP(CPU* cpu){ //L
    set_register(cpu, PC, get_register(cpu, NN));
}

void ins_JP_OP(CPU* cpu, Flag flag, Reg r){ //L
    I16 a = get_register(cpu, NN);
    I16 f = get_register(cpu, F);
    if(r != HL){
        switch (flag) {
        case zz:
            if(get_flag(cpu, zz)) set_register(cpu, PC, a);
            break;
        case nn:
            if(!get_flag(cpu, zz)) set_register(cpu, PC, a);
            break;
        case cc:
            if(get_flag(cpu, cc)) set_register(cpu, PC, a);
            break;
        case hh:
            if(!get_flag(cpu, cc)) set_register(cpu, PC, a);
            break;
        default:
            break;
        }
    }else{
        set_register(cpu, PC, get_register(cpu, HL));
    }
}

void ins_JR(CPU* cpu){ //L
    set_register(cpu, PC, get_register(cpu, PC) + get_register(cpu, N));
}

void ins_JR_OP(CPU* cpu, Flag flag){ //L
    I16 a = get_register(cpu, PC) + get_register(cpu, N);
    I16 f = get_register(cpu, F);
        switch (flag)
        {
        case zz:
            if(get_flag(cpu, zz)) set_register(cpu, PC, a);
            break;
        case nn:
            if(!get_flag(cpu, zz)) set_register(cpu, PC, a);
            break;
        case cc:
            if(get_flag(cpu, cc)) set_register(cpu, PC, a);
            break;
        case hh:
            if(!get_flag(cpu, cc)) set_register(cpu, PC, a);
            break;
        default:
            break;
        }
}

void ins_POP(CPU* cpu, Reg r){      //POP as PUSH, restore full register
    I16 v = read_mem(cpu, get_register(cpu, SP)) | read_mem(cpu, get_register(cpu, SP+1)) << 8;
    *cpu->pc += 2;
    set_register(cpu, r,v);
}

void ins_PUSH(CPU* cpu, Reg r){
    I16 v = get_register(cpu, r);
    set_mem(cpu, *cpu->sp + 1, v & 0x00FF);
    set_mem(cpu, *cpu->sp + 2, v & 0xFF00);
    *cpu->sp += 2;
}

/******************
 * 3.3.10 RESTART *
 ******************/

void ins_RST(CPU* cpu, int n){ //L
    ins_PUSH(cpu, PC);
    set_register(cpu, PC, 0x0 + n);
}

/******************
 * 3.3.11 RETURNS *
 ******************/
void ins_RET(CPU* cpu){ //L
    ins_POP(cpu, PC);
}

void ins_RET_OP(CPU* cpu, Flag flag){   //L
        switch (flag)
        {
        case zz:
            if(get_flag(cpu, zz)) ins_POP(cpu, PC);
            break;
        case nn:
            if(!get_flag(cpu,zz)) ins_POP(cpu, PC);
            break;
        case cc:
            if(get_flag(cpu, cc)) ins_POP(cpu, PC);
            break;
        case hh:
            if(!get_flag(cpu, cc)) ins_POP(cpu, PC);
            break;
        default:
            break;
        }
}

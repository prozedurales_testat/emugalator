#include <stdio.h>
#include <stdlib.h>
#define FORBIDDEN_INDEX -11

/*********************************************************
 *      The CPU and Memory module. Exports the CPU struct
 *      Author: Pelle Hänel
 *      Date: *2020-12-24
 *********************************************************/


typedef u_int8_t   I8;
typedef u_int16_t I16;
typedef u_int32_t I32;
typedef u_int64_t I64;

typedef enum{ A, B, C, D, E, F, H, L, AF, BC, DE, HL, SP, PC, RAF, RBC, RDE, RHL, N, NN} Reg;
typedef enum{cc, hh, nn, zz} Flag;

typedef struct cpu_ {
    //registers:
    // deal with the pairings by using a getter function FIXME:
    //    implement access always using getter setter? DISCUSS!

    //General purpose registers:
    //I16* A, B, C, D, E, F, H, L, AF, BC, DE, HL, SP, PC
    I16* regs;
    //Stack pointer, base pointer
    I16* sp;
    //Program counter
    I16* pc;
    //Processor clock
    I64 CLCK;
    //RAM
    I8* ram;
}CPU;

I8 read_mem(CPU* cpu, I16 addr){
    return cpu->ram[addr];
}

void set_mem(CPU* cpu, I16 addr, I8 v){
    cpu->ram[addr] = v;
}

void increase_pc(CPU* cpu){
    ++(*(cpu->pc));
}

void decrease_pc(CPU* cpu){
    --(*(cpu->pc));
}

I16 get_register(CPU* cpu, Reg r){
    switch (r) {
    case A:
        return cpu->regs[0];
        break;
    case B:
        return cpu->regs[1];
        break;
    case C:
        return cpu->regs[2];
        break;
    case D:
        return cpu->regs[3];
        break;
    case E:
        return cpu->regs[4];
        break;
    case F:
        return cpu->regs[5];
        break;
    case H:
        return cpu->regs[6];
        break;
    case L:
        return cpu->regs[7];
        break;
    case AF:
        return (cpu->regs[0] << 8) + cpu->regs[5];
        break;
    case BC:
        return (cpu->regs[1] << 8) + cpu->regs[2];
        break;
    case DE:
        return (cpu->regs[3] << 8) + cpu->regs[4];
        break;
    case HL:
        return (cpu->regs[6] << 8) + cpu->regs[7];
        break;
    case RAF:
        return read_mem(cpu, (cpu->regs[0] << 8) + cpu->regs[5]);
        break;
    case RBC:
        return read_mem(cpu, (cpu->regs[1] << 8) + cpu->regs[2]);
        break;
    case RDE:
        return read_mem(cpu, (cpu->regs[3] << 8) + cpu->regs[4]);
        break;
    case RHL:
        return read_mem(cpu, (cpu->regs[6] << 8) + cpu->regs[7]);
        break;
    case N:
        increase_pc(cpu);
        return read_mem(cpu,*(cpu->pc));
        break;
    case NN:
        increase_pc(cpu);
        return read_mem(cpu,*(cpu->pc)) + (read_mem(cpu,++(*(cpu->pc))) << 8);
        break;
    case SP:
        return *(cpu->sp);
        break;
    case PC:
        return *(cpu->pc);
        break;
    default:
        return FORBIDDEN_INDEX;
        break;
    }
}


void set_register(CPU* cpu, Reg r, I16 val){
    switch (r)
    {
    case A:
        cpu->regs[0] = val;
        break;
    case B:
        cpu->regs[1] = val;
        break;
    case C:
        cpu->regs[2] = val;
        break;
    case D:
        cpu->regs[3] = val;
        break;
    case E:
        cpu->regs[4] = val;
        break;
    case F:
        cpu->regs[5] = val;
        break;
    case H:
        cpu->regs[6] = val;
        break;
    case L:
        cpu->regs[7] = val;
        break;
    case AF:
        cpu->regs[0] = (0xFF00 & val) >> 8;
        cpu->regs[5] = 0x00FF & val;
        break;
    case BC:
        cpu->regs[1] = (0xFF00 & val) >> 8;
        cpu->regs[2] = 0x00FF & val;
        break;
    case DE:
        cpu->regs[3] = (0xFF00 & val) >> 8;
        cpu->regs[4] = 0x00FF & val;
        break;
    case HL:
        cpu->regs[6] = (0xFF00 & val) >> 8;
        cpu->regs[7] = 0x00FF & val;
        break;
    case RAF:
        set_mem(cpu, (cpu->regs[0] << 8) + cpu->regs[5], (I8) val);
        break;
    case RBC:
        set_mem(cpu, (cpu->regs[1] << 8) + cpu->regs[2], (I8) val);
        break;
    case RDE:
        set_mem(cpu, (cpu->regs[3] << 8) + cpu->regs[4], (I8) val);
    case RHL:
        set_mem(cpu, (cpu->regs[6] << 8) + cpu->regs[7], (I8) val);
        break;
    case SP:
        *(cpu->sp) = val;
        break;
    case PC:
        *(cpu->pc) = val;
        break;
    default:
        break;
    }
}

void clean_register(CPU* cpu){
    for (int i = 0; i < 8; i++) {
        cpu->regs[i] = cpu->regs[i] & 0xFF;
    }
}

CPU* init_cpu(){
    CPU* cpu = calloc(1, sizeof(CPU));
    cpu -> ram = calloc(8192, sizeof(I8));
    cpu -> regs = calloc(8, sizeof(I16));
    cpu -> sp = calloc(1, sizeof(I16));
    *cpu->sp = 0;
    cpu -> pc = calloc(1, sizeof(I16));
    return cpu;
}

void set_flag(CPU* cpu, Flag f, I8 v){
    I8 t;
    switch (f)
    {
    case zz:
        t = get_register(cpu, F);
        t &= 0b01111111;
        t |= v << 7;
        set_register(cpu, F, t);
        break;
     case nn:
        t = get_register(cpu, F);
        t &= 0b10111111;
        t |= v << 6;
        set_register(cpu, F, t);
        break;
     case hh:
        t = get_register(cpu, F);
        t &= 0b11011111;
        t |= v << 5;
        set_register(cpu, F, t);
        break;
     case cc:
        t = get_register(cpu, F);
        t &= 0b11101111;
        t |= v << 4;
        set_register(cpu, F, t);
        break;
    default:
        break;
    }
}

I8 get_flag(CPU* cpu, Flag f){
    return ((get_register(cpu, F) & (1 << (f + 4))) >> (f + 4));
}



// 0b1010_0000 & 0b0000001